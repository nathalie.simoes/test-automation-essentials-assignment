const request = require('supertest')
const { app } = require('../../server')
const { firstNames, lastNames, generatePerson, capitalizeAll, formatPersonData } = require('../../src/personGenerator')

// narrow testing
test('generates a random person', () => {
    const person = generatePerson()
    const [personFirstName, personLastName] = person.name.split(" ")

    expect(firstNames).toContain(personFirstName)
    expect(lastNames).toContain(personLastName)
    expect(person.age).toBeGreaterThanOrEqual(1)
    expect(person.age).toBeLessThanOrEqual(100)
    expect(person.height).toBeGreaterThanOrEqual(1)
    expect(person.height).toBeLessThanOrEqual(2)
})

test('formats person data', () => {
    const formattedPerson = formatPersonData()

    const [full, decimal] = formattedPerson.height.split(".")
    expect(['1', '2']).toContain(full)
    expect(decimal.length).toBe(2)

    const [firstName, lastName] = formattedPerson.name.split(" ")
    expect(firstName.charAt(0)).toMatch(/^[A-Z]*$/)
    expect(lastName.charAt(0)).toMatch(/^[A-Z]*$/)
})

// broad testing
test('GET /people', async () => {
    const call = await request(app).get('/people').expect(200)
    expect(call.statusCode).toEqual(200)
    expect(call.body).toBeDefined()
    expect(call.body).toBeInstanceOf(Array)
    expect(call.body.length).toEqual(10)
})
