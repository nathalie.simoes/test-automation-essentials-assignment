const puppeteer = require('puppeteer')

test('should create an element with generated person data', async () => {
  const browser = await puppeteer.launch({ headless: true, slowMo: 80 })
  const page = await browser.newPage()
  await page.goto('http://localhost:8080')
  await page.click('#btnGenerator')
  const finalText = await page.$eval('.person-info', el => el.textContent)
  expect(finalText).toBeTruthy()
})