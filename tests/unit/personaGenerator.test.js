const { capitalizeAll, firstNames, lastNames, generateAge, generateHeight, generateFullName } = require('../../src/personGenerator')

test('when given a string to capitalizeAll', () => {
    expect(capitalizeAll('foo bar')).toBe('Foo Bar')
})

test('generates a random name from arrays of first and last names', () => {
    const [firstName, lastName] = generateFullName().split(" ")

    expect(firstNames).toContain(firstName)
    expect(lastNames).toContain(lastName)
})

test('generates a random age between 1 and 100', () => {
    expect(generateAge()).toBeGreaterThanOrEqual(1)
    expect(generateAge()).toBeLessThanOrEqual(100)
})

test('generates a random height between 1 and 2', () => {
    const height = generateHeight()

    expect(height).toBeGreaterThanOrEqual(1)
    expect(height).toBeLessThanOrEqual(2)
})
