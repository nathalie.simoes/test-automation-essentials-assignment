const firstNames = [
  'miguel',
  'arthur',
  'davi',
  'gabriel',
  'maria',
  'alice',
  'heitor',
  'pedro',
  'laura',
  'sophia',
]

const lastNames = [
  'silva',
  'souza',
  'costa',
  'santos',
  'oliveira',
  'pereira',
  'rodrigues',
  'almeida',
  'nascimento',
  'lima',
  'araújo',
  'fernandes',
  'carvalho',
  'gomes',
  'martins',
  'rocha',
  'ribeiro',
  'alves',
  'monteiro',
  'mendes',
  'barros',
  'freitas',
  'barbosa',
  'pinto',
  'moura',
  'cavalcanti',
  'dias',
  'castro',
  'campos',
  'cardoso',
]

const capitalizeAll = (text) => {
    return text.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase())
}

const generateFullName = () => {
    const firstName = firstNames[Math.floor(Math.random() * firstNames.length)]
    const lastName = lastNames[Math.floor(Math.random() * lastNames.length)]

    return `${firstName} ${lastName}`
}

const generateAge = () => {
    return Math.floor(Math.random() * 100) + 1
}

const generateHeight = () => {
    return Math.random() * (2 - 1) + 1
}

const generatePerson = () => {
    let person = {
        name: generateFullName(),
        age: generateAge(),
        height: generateHeight(),
    }

    return person
}

const formatPersonData = () => {
    const person = generatePerson()

    person.name = capitalizeAll(person.name)
    person.height = person.height.toFixed(2)
    return person
}

exports.firstNames = firstNames
exports.lastNames = lastNames
exports.capitalizeAll = capitalizeAll
exports.generateFullName = generateFullName
exports.generateAge = generateAge
exports.generateHeight = generateHeight
exports.generatePerson = generatePerson
exports.formatPersonData = formatPersonData
