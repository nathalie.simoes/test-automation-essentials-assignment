const express = require('express')
const app = express()
const path = require('path')
const { formatPersonData } = require('./src/personGenerator')

app.use("/", express.static(path.resolve(__dirname, "src", "")))

app.get('/people', async (req, res) => {
  try{
    const people = new Array(10).fill({}).map(() => formatPersonData())
    res.json(people)
  }catch(err){
    res.json({ error: err })
  }
})

app.listen(8080)

exports.app = app